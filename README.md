# track-units

Track spike sorted units across multiple recording sessions. This package is built on top of 
[SpikeInterface](https://spikeinterface.readthedocs.io/en/latest/).

## Installation

Clone the repository and install as follows:

```bash
git clone https://git.bsse.ethz.ch/abuccino/track-units.git
cd track-units 
pip install .
```

## Usage

The `track_units` package uses the `TrackUnits` class to perform unit tracking. The input are two `SortingExtractor` 
objects, and optionally the channel locations. 

**IMPORTANT**: the `SortingExtractor` objects must have the `template` property. In order to compute the templates, you
can use the [`spiketoolkit.postprocessing.get_unit_template()` function](https://spikeinterface.readthedocs.io/en/latest/api.html#module-spiketoolkit.postprocessing)

```python
from track_units import TrackUnits

# sorting1 and sorting2 are SortingExtrator objects with the `template` property
track = TrackUnits(sorting1, sorting2, locations=recording_locations)

# unit ids of matches
display(track.matched_units)

# plot templates
track.plot_tracked_templates()
```
