import numpy as np
import pandas as pd
from scipy.optimize import linear_sum_assignment


def make_possible_match(dissimilarity_scores, max_dissimilarity):
    """
    Given an agreement matrix and a max_dissimilarity threhold.
    Return as a dict all possible match for each template in each side.

    Note : this is symmetric.


    Parameters
    ----------
    dissimilarity_scores: pd.DataFrame

    max_dissimilarity: float


    Returns
    -----------
    best_match_12: pd.Series

    best_match_21: pd.Series

    """
    unit1_ids = np.array(dissimilarity_scores.index)
    unit2_ids = np.array(dissimilarity_scores.columns)

    # threhold the matrix
    scores = dissimilarity_scores.values.copy()
    scores[scores > max_dissimilarity] = np.inf

    possible_match_12 = {}
    for i1, u1 in enumerate(unit1_ids):
        inds_match = np.isfinite(scores[i1, :])
        possible_match_12[u1] = unit2_ids[inds_match]

    possible_match_21 = {}
    for i2, u2 in enumerate(unit2_ids):
        inds_match = np.isfinite(scores[:, i2])
        possible_match_21[u2] = unit1_ids[inds_match]

    return possible_match_12, possible_match_21


def make_best_match(dissimilarity_scores, max_dissimilarity):
    """
    Given an agreement matrix and a max_dissimilarity threshold.
    return a dict a best match for each units independently of others.

    Note : this is symmetric.

    Parameters
    ----------
    dissimilarity_scores: pd.DataFrame

    max_dissimilarity: float


    Returns
    -----------
    best_match_12: pd.Series

    best_match_21: pd.Series


    """
    unit1_ids = np.array(dissimilarity_scores.index)
    unit2_ids = np.array(dissimilarity_scores.columns)

    scores = dissimilarity_scores.values.copy()

    best_match_12 = pd.Series(index=unit1_ids, dtype='int64')
    for i1, u1 in enumerate(unit1_ids):
        ind_min = np.argmin(scores[i1, :])
        if scores[i1, ind_min] <= max_dissimilarity:
            best_match_12[u1] = unit2_ids[ind_min]
        else:
            best_match_12[u1] = -1

    best_match_21 = pd.Series(index=unit2_ids, dtype='int64')
    for i2, u2 in enumerate(unit2_ids):
        ind_min = np.argmin(scores[:, i2])
        if scores[ind_min, i2] <= max_dissimilarity:
            best_match_21[u2] = unit1_ids[ind_min]
        else:
            best_match_21[u2] = -1

    return best_match_12, best_match_21


def make_hungarian_match(dissimilarity_scores, max_dissimilarity):
    """
    Given an agreement matrix and a max_dissimilarity threshold.
    return the "optimal" match with the "hungarian" algo.
    This use internally the scipy.optimze.linear_sum_assignment implementation.

    Parameters
    ----------
    dissimilarity_scores: pd.DataFrame

    max_dissimilarity: float


    Returns
    -----------
    hungarian_match_12: pd.Series

    hungarian_match_21: pd.Series

    """
    unit1_ids = np.array(dissimilarity_scores.index)
    unit2_ids = np.array(dissimilarity_scores.columns)

    # threhold the matrix
    scores = dissimilarity_scores.values.copy()

    [inds1, inds2] = linear_sum_assignment(scores)

    hungarian_match_12 = pd.Series(index=unit1_ids, dtype='int64')
    hungarian_match_12[:] = -1
    hungarian_match_21 = pd.Series(index=unit2_ids, dtype='int64')
    hungarian_match_21[:] = -1

    for i1, i2 in zip(inds1, inds2):
        u1 = unit1_ids[i1]
        u2 = unit2_ids[i2]
        if dissimilarity_scores.at[u1, u2] < max_dissimilarity:
            hungarian_match_12[u1] = u2
            hungarian_match_21[u2] = u1

    return hungarian_match_12, hungarian_match_21


def dissimilarity(t0, t1, t0_inds, t1_inds, max_channels, min_shared_channels=5):
    """
    Computes dissimilarity between pairs of templates

    Parameters
    ----------
    t0: np.array
    t1: np.array
    t0_inds: np.array
    t1_inds: np.array
    max_channels: int
    min_shared_channels: int

    Returns
    -------
    dissimilarity: float

    """
    from scipy.spatial.distance import cosine
    if t0_inds is None:
        t0_l = np.ravel(t0)
        t1_l = np.ravel(t1)
        return cosine(t0_l, t1_l)
    else:
        shared_channel_idxs = [ch for ch in t0_inds if ch in t1_inds]  # ch<0 is for channels empty, label -1
        if len(shared_channel_idxs) > min_shared_channels:
            # reorder channels
            shared_channel_idxs = np.array(shared_channel_idxs)
            reorder_t_ind_i = np.zeros(len(shared_channel_idxs), dtype='int')
            reorder_t_ind_j = np.zeros(len(shared_channel_idxs), dtype='int')
            for s, sc in enumerate(shared_channel_idxs):
                reorder_t_ind_i[s] = np.where(t0_inds == sc)[0]
                reorder_t_ind_j[s] = np.where(t1_inds == sc)[0]
            t_0_shared = t0[t0_inds[reorder_t_ind_i]]
            t_1_shared = t1[t1_inds[reorder_t_ind_j]]
            t0_l = np.ravel(t_0_shared)
            t1_l = np.ravel(t_1_shared)
            cos_dist = cosine(t0_l, t1_l) * (max_channels / len(shared_channel_idxs))
            return cos_dist
        else:
            # no channels are shared
            return 2 * max_channels / min_shared_channels