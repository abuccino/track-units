import numpy as np
import spikeextractors as se
import matplotlib.pyplot as plt
from copy import deepcopy
from .trackunits_utils import *


class TrackTemplates:
    """
    Base class to match units based on template similarity
    """

    def __init__(self, templates1, templates2, locations1=None, locations2=None, unit_ids1=None, unit_ids2=None,
                 max_channels=None, max_dissimilarity=0.5, min_shared_channels=5, dissimilarity_function=None,
                 do_matching=True, verbose=False):

        if max_dissimilarity is None:
            if max_channels is not None:
                self.max_dissimilarity = max_channels / 2
            else:
                self.max_dissimilarity = 10
        else:
            self.max_dissimilarity = max_dissimilarity
        self.dissimilarity_function = dissimilarity_function
        self.max_channels = max_channels
        self.min_shared_channels = min_shared_channels
        self.locations = None
        self._verbose = verbose
        self.original_templates = [templates1, templates2]

        if locations1 is None and locations2 is None:
            assert templates1.shape[1] == templates2.shape[1]
        else:
            if locations1 is not None and locations2 is None:
                assert templates1.shape[1] == templates2.shape[1]
                self.locations = [locations1, locations1]
            elif locations1 is None and locations2 is not None:
                assert templates1.shape[1] == templates2.shape[1]
                self.locations = [locations2, locations2]
            else:
                # load templates and locations
                self.locations = [locations1, locations2]

                # find common locations and merge templates
                locations_tuple = [[tuple(l) for l in locs] for locs in self.locations]
                loc_intersect = list(set(locations_tuple[0]) & set(locations_tuple[1]))
                print(f"Common locations {len(list(loc_intersect))}")

                # cut templates on overlapping locations
                common_idxs1 = []
                common_idxs2 = []
                for common_loc in loc_intersect:
                    common_loc = tuple(common_loc)

                    if common_loc in locations_tuple[0]:
                        common_idxs1.append(locations_tuple[0].index(common_loc))
                    if common_loc in locations_tuple[1]:
                        common_idxs2.append(locations_tuple[1].index(common_loc))

                templates1 = templates1[:, np.array(common_idxs1)]
                templates2 = templates2[:, np.array(common_idxs2)]

        self.templates = [templates1, templates2]

        self.matches = dict()

        if unit_ids1 is None:
            unit_ids1 = np.arange(len(templates1))
        else:
            assert len(unit_ids1) == len(templates1)

        if unit_ids2 is None:
            unit_ids2 = np.arange(len(templates2))
        else:
            assert len(unit_ids2) == len(templates2)
        self.unit_ids = [unit_ids1, unit_ids2]

        if max_channels is not None:
            templates_inds_1 = []
            templates_inds_2 = []

            for template in templates1:
                if len(template) > max_channels:
                    amps = np.max(np.abs(template), 1)
                    temp_inds = np.argsort(amps)[::-1][:max_channels]
                    templates_inds_1.append(temp_inds)
                else:
                    templates_inds_1.append(np.arange(len(template)))
            for template in templates2:
                if len(template) > max_channels:
                    amps = np.max(np.abs(template), 1)
                    temp_inds = np.argsort(amps)[::-1][:max_channels]
                    templates_inds_1.append(temp_inds)
                else:
                    templates_inds_2.append(np.arange(len(template)))
            templates_inds_1 = np.array(templates_inds_1)
            templates_inds_2 = np.array(templates_inds_2)
        else:
            templates_inds_1 = None
            templates_inds_2 = None

        self.template_inds = [templates_inds_1, templates_inds_2]

        self._do_dissimilarity()
        if do_matching:
            self._do_matching()

    @property
    def matched_units(self):
        matched_units = deepcopy(self.matches['hungarian_match_01'][self.matches['hungarian_match_01'] != -1])
        return matched_units

    def perform_match(self, max_dissimilarity=None, min_shared_channels=None):
        do_matching = False
        if max_dissimilarity is not None:
            self.max_dissimilarity = max_dissimilarity
            self._do_dissimilarity()
            do_matching = True
        if min_shared_channels is not None:
            self.min_shared_channels = min_shared_channels
            do_matching = True

        if do_matching:
            self._do_matching()

    def _do_dissimilarity(self):
        if self._verbose:
            print('Agreement scores...')

        # agreement matrix score for each pair
        self.matches['dissimilarity_scores'] = self._make_dissimilary_matrix()

    def _do_matching(self):
        # must be implemented in subclass
        if self._verbose:
            print("Matching...")

        self.matches['possible_match_01'], self.matches['possible_match_10'] = \
            make_possible_match(self.matches['dissimilarity_scores'], self.max_dissimilarity)
        self.matches['best_match_01'], self.matches['best_match_10'] = \
            make_best_match(self.matches['dissimilarity_scores'], self.max_dissimilarity)
        self.matches['hungarian_match_01'], self.matches['hungarian_match_10'] = \
            make_hungarian_match(self.matches['dissimilarity_scores'], self.max_dissimilarity)

    def _make_dissimilary_matrix(self):
        templates_0 = self.templates[0]
        templates_1 = self.templates[1]
        template_inds_0 = self.template_inds[0]
        template_inds_1 = self.template_inds[1]
        diss_matrix = np.zeros((len(templates_0), len(templates_1)))

        # take care of missing channels
        if template_inds_0 is None and template_inds_1 is None:
            assert templates_0.shape[1] == templates_1.shape[1]
            template_inds_0 = [None for i in range(len(templates_0))]
            template_inds_1 = [None for i in range(len(templates_1))]

        t1_iter = range(len(templates_0))
        t2_iter = range(len(templates_1))

        for i in t1_iter:
            for j in t2_iter:
                t0 = templates_0[i]
                t1 = templates_1[j]
                t0_ind = template_inds_0[i]
                t1_ind = template_inds_1[j]
                diss = dissimilarity(t0, t1, t0_ind, t1_ind,
                                     self.max_channels, min_shared_channels=self.min_shared_channels)
                # if not np.isnan(diss):
                diss_matrix[i, j] = diss
            # el    se:
                #     diss_matrix[i, j] = 2

        if self.max_channels is None:
            # divide by max cosine distance
            diss_matrix /= 2
        else:
            # divide by max theoretical value
            diss_matrix /= (2 * (self.max_channels / self.min_shared_channels))

        diss_matrix = pd.DataFrame(
            diss_matrix,
            index=self.unit_ids[0],
            columns=self.unit_ids[1])

        return diss_matrix

    def plot_tracked_templates(self, cmap=None):
        assert self.locations is not None, \
            "To use the 'plot_tracked_templates' provide the locations with the set_locations() function"

        try:
            import axon_velocity as av
        except:
            raise ImportError("Install axon_velocity")

        vscale = 1.5 * np.max([np.max(self.original_templates[0]), np.max(self.original_templates[1])])

        fig, axs = plt.subplots(nrows=2)

        matched_units = self.matches['hungarian_match_01'][self.matches['hungarian_match_01'] != -1]
        num_matched_units = len(matched_units)
        if cmap is not None:
            cm = plt.get_cmap(cmap)
            colors = [cm(i / num_matched_units) for i in range(num_matched_units)]

        for i, (u, tr) in enumerate(matched_units.items()):
            if cmap is None:
                color = f"C{np.mod(i, 10)}"
            else:
                color = colors[i]

            idx1 = list(self.unit_ids[0]).index(u)
            idx2 = list(self.unit_ids[1]).index(tr)

            axs[0] = av.plot_template(self.original_templates[0][idx1], self.locations[0], ax=axs[0], colors=color,
                                      vscale=vscale, lw=1)
            axs[1] = av.plot_template(self.original_templates[1][idx2], self.locations[1], ax=axs[1], colors=color,
                                      vscale=vscale, lw=1)

        min_x = min([min(self.locations[0][:, 0]), min(self.locations[1][:, 0])])
        min_y = min([min(self.locations[0][:, 1]), min(self.locations[1][:, 1])])
        max_x = max([max(self.locations[0][:, 0]), max(self.locations[1][:, 0])])
        max_y = max([max(self.locations[0][:, 1]), max(self.locations[1][:, 1])])

        if min_x != max_x:
            range_x = max_x - min_x
            axs[0].set_xlim(min_x - 0.2 * range_x, max_x + 0.2 * range_x)
            axs[1].set_xlim(min_x - 0.2 * range_x, max_x + 0.2 * range_x)

        if min_y != max_y:
            range_y = max_y - min_y
            axs[0].set_ylim(min_y - 0.2 * range_y, max_y + 0.2 * range_y)
            axs[1].set_ylim(min_y - 0.2 * range_y, max_y + 0.2 * range_y)

        axs[0].set_title("Templates 1")
        axs[1].set_title("Templates 2")

        return fig


class TrackUnits(TrackTemplates):
    """
    Class to find matched units in two SortingExtractor objects based on template similarity.
    """
    def __init__(self, sorting1, sorting2, locations1=None, locations2=None, max_channels=None,
                 max_dissimilarity=0.5, min_shared_channels=5, dissimilarity_function=None,
                 do_matching=True, verbose=False):

        if max_dissimilarity is None:
            if max_channels is not None:
                self.max_dissimilarity = max_channels / 2
            else:
                self.max_dissimilarity = 10
        else:
            self.max_dissimilarity = max_dissimilarity
        self.dissimilarity_function = dissimilarity_function
        self.max_channels = max_channels
        self.min_shared_channels = min_shared_channels
        self._sorting1 = sorting1
        self._sorting2 = sorting2
        self._verbose = verbose

        unit_ids1 = sorting1.get_unit_ids()
        unit_ids2 = sorting2.get_unit_ids()

        if 'template' in sorting1.get_shared_unit_property_names():
            templates1 = np.array(sorting1.get_units_property(unit_ids=sorting1.get_unit_ids(),
                                                              property_name='template'))
        else:
            raise Exception("The 'template' property hasn't been added to the sorting1 output!")

        if 'template' in sorting2.get_shared_unit_property_names():
            templates2 = np.array(sorting2.get_units_property(unit_ids=sorting2.get_unit_ids(),
                                                              property_name='template'))
        else:
            raise Exception("The 'template' property hasn't been added to the sorting2 output!")

        TrackTemplates.__init__(self, templates1, templates2, locations1=locations1, locations2=locations2,
                                unit_ids1=unit_ids1, unit_ids2=unit_ids2, max_channels=max_channels,
                                max_dissimilarity=max_dissimilarity, min_shared_channels=min_shared_channels,
                                dissimilarity_function=dissimilarity_function,
                                do_matching=do_matching, verbose=verbose)
